build:
	GOARCH=amd64 GOOS=linux go build -o ./bin/metadata ./metadata/cmd/main.go
	GOARCH=amd64 GOOS=linux go build -o ./bin/rating ./rating/cmd/main.go
	GOARCH=amd64 GOOS=linux go build -o ./bin/movie ./movie/cmd/main.go

run: build
	./bin/metadata & ./bin/rating & ./bin/movie && fg

test:
	go test ./...

test_coverage:
	go test ./... -coverprofile=coverage.out

dep:
	go mod download
