package http

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"

	"movieexample.com/metadata/pkg/model"
	"movieexample.com/movie/internal/gateway"
	"movieexample.com/pkg/discovery"
)

type Gateway struct {
	registry discovery.Registry
}

func New(registry discovery.Registry) *Gateway {
	return &Gateway{registry}
}

func (g *Gateway) Get(ctx context.Context, id string) (*model.Metadata, error) {
	addresses, err := g.registry.ServiceAddresses(ctx, "metadata")
	if err != nil {
		return nil, err
	}
	url := "http" + "://" + addresses[rand.Intn(len(addresses))] + "/metadata"
	log.Printf("Calling metadata service. Request: GET %s", url)
	rq, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}
	rq = rq.WithContext(ctx)
	values := rq.URL.Query()
	values.Add("id", id)
	rq.URL.RawQuery = values.Encode()
	rs, err := http.DefaultClient.Do(rq)
	if err != nil {
		return nil, err
	}
	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(rs.Body)
	if rs.StatusCode == http.StatusNotFound {
		return nil, gateway.ErrNotFound
	} else if rs.StatusCode/100 != 2 {
		return nil, fmt.Errorf("non-2xx response: %v", rs)
	}
	var v *model.Metadata
	if err := json.NewDecoder(rs.Body).Decode(&v); err != nil {
		return nil, err
	}
	return v, nil
}
