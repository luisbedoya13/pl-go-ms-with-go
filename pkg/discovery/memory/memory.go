package memory

import (
	"context"
	"errors"
	"sync"
	"time"

	"movieexample.com/pkg/discovery"
)

type serviceInstance struct {
	hostPort   string
	lastActive time.Time
}

type Registry struct {
	sync.RWMutex
	serviceAddresses map[string]map[string]*serviceInstance
}

func NewRegistry() *Registry {
	return &Registry{serviceAddresses: make(map[string]map[string]*serviceInstance)}
}

func (r *Registry) Register(_ context.Context, instanceID string, serviceName string, hostPort string) error {
	r.Lock()
	defer r.Unlock()
	if _, ok := r.serviceAddresses[serviceName]; !ok {
		r.serviceAddresses[serviceName] = make(map[string]*serviceInstance)
	}
	r.serviceAddresses[serviceName][instanceID] = &serviceInstance{hostPort, time.Now()}
	return nil
}

func (r *Registry) Deregister(_ context.Context, instanceID string, serviceName string) error {
	r.Lock()
	defer r.Unlock()
	if _, ok := r.serviceAddresses[serviceName]; !ok {
		return nil
	}
	delete(r.serviceAddresses[serviceName], instanceID)
	return nil
}

func (r *Registry) ReportHealthyState(instanceID string, serviceName string) error {
	r.Lock()
	defer r.Unlock()
	if _, ok := r.serviceAddresses[serviceName]; !ok {
		return errors.New("service is not registered yet")
	}
	if _, ok := r.serviceAddresses[serviceName][instanceID]; !ok {
		return errors.New("service instance is not registered yet")
	}
	r.serviceAddresses[serviceName][instanceID].lastActive = time.Now()
	return nil
}

func (r *Registry) ServiceAddresses(_ context.Context, serviceName string) ([]string, error) {
	r.Lock()
	defer r.Unlock()
	if len(r.serviceAddresses[serviceName]) == 0 {
		return nil, discovery.ErrNotFound
	}
	var res []string
	for _, i := range r.serviceAddresses[serviceName] {
		if i.lastActive.Before(time.Now().Add(-5 * time.Second)) {
			continue
		}
		res = append(res, i.hostPort)
	}
	return res, nil
}
